\chapter{Conclusion}
\section{Thesis summary}
In this thesis, we develop deep learning architectures for modelling building energy consumption and air quality.
Using historic data, we propose to optimize energy demand, while improving indoor comfort and well being.
Because modelling the behavior of a building is a complex task, where many of the relevant factors are unknown, we quantify the uncertainty associated with each prediction of our models.

When simulating buildings behaviors, we show in Chapter~\ref{chap:metamodel} that we can replace numerical simulators such as TRNSYS with statistical surrogate models.
Once trained, they offer much faster computation times, which allows us to run costly optimization tasks in a reasonable time frame.
We present an application on two real buildings handled by Oze-Energies.
The unknown parameters of the building, such as physical properties or HVAC usages, are estimated by comparing the output of the model with the historic data gathered.
Then, energy loads are optimized by evaluating simulated scenarios from the metamodel, and selecting a set of HVAC settings leading to a reduced consumption for an equivalent thermic comfort.
This experiment demonstrates that results are coherent between the metamodel and the original physical simulator TRNSYS, throughout the calibration and optimization tasks.
Additionally, the speed of the metamodel, and its ability to parallelize dozen of simulations, allows us to calibrate buildings that would have taken days using TRNSYS.

After demonstrating the performance of statistical models to approximate complex functions, we explore methods for quantifying their uncertainty.
In a context where input variables may be noisy or unavailable, predicting a single point estimate cannot reflect the potential uncertainty ; instead, we model the distribution of the observations, from which we can quantify the uncertainty.
Well known statistical models, such as Hidden Markov Model, fit this description, however their training procedure usually do not scale well to high number of parameters.
In Chapter~\ref{chap:uncertainty-modelling}, we propose two approaches for combining the approximation capabilities of neural networks, with uncertainty modelling techniques.

We start by modelling noise on the recurrent layers of neural networks, whose parameters are estimated using Sequential Monte Carlo (SMC) methods.
As it is sufficient to constraint this modelling to the last layer of the model only, we develop a decoupled deep learning architecture adapted to time series.
The parameters of the model are first estimated in an efficient, deterministic gradient descent.
Then, the weights of the last layer are finetuned by minimizing the log likelihood associated with a set of weighted particles.
On a relative humidity forecasting task, our model produces accurate predictions along with confidence intervals.

In a second approach, we propose a quantized latent model.
Recent advances in generative modelling have pushed towards these models, as they can lead to a more meaningful representation of the data, without degrading performance.
In our case, we are able to simplify the training procedure, as we no longer rely on the complex approximation of SMC methods.
Furthermore, we show that interpreting the discrete latent states paves the way for new applications, such as unsupervised segmentation for instance.
The parameters of the model are estimated by Variational Inference, by maximizing a lower bound on the likelihood.
Performance wise, this new discrete based model is on par with its continuous counterpart on the relative humidity forecasting task.

The last part of this thesis focuses on modelling discrete distributions, in the same context of quantized latent states.
In Chapter~\ref{chap:latent-distributions}, we experiment with several choices of prior models, as they are at the center of the discrete latent generative models literature.
State of the art architectures rely on complex autoregressive priors, which require several implementation tricks for their training procedure to converge.
We propose a theoretically grounded methodology for modelling the discrete latent state distribution, regardless of the nature of the observations (time series, images, etc.), by using diffusion bridges.
The key idea is to iteratively corrupt a complex distribution into a non informative one, by adding noise, then learning the inverse reconstruction process, bridging the gap between both distributions.
Our experiments show the relevance of this new methodology on computer vision and time series tasks.

Finally, we propose an improvement of the noising process of the diffusion bridge.
Because of the high dimension of the latent vectors space, adding independent noise in all dimension quickly causes scaling and geometric problems.
By computing instead a covariance matrix as a function of relevant latent vectors, we are able to better direct the noising process towards a non informative distribution.
Our new model is much faster to converge during training, reducing the number of epochs by a factor of 10.

\section{Perspectives}
\paragraph{Metamodelling and energy load optimization. }
The end-to-end metamodelling methodology proposed in Chapter~\ref{chap:metamodel} was satisfactory on two well chosen buildings, and introduced new improvement perspectives for future works on more complex behavior modelling.

For instance, during the training of the metamodel, we defined the loss as a function of a hyper parameter $\beta$, which we were not able to tune.
Regardless, the results of the metamodel were satisfactory, yet exploring how to balance the consumption error criteria with the indoor temperature one may be required to obtain more precise results, especially during calibration.
We believe this parameter can only be tuned by discussing with energy managers, as they are the most aware of the importance in the accuracy of each variable.
Similarly, when optimizing energy loads, we only considered the total cumulated consumption of a building, without pondering any particular behavior.
While implementing more complex objectives is not a limitation, defining which behavior to reward (low instant power consumption, heating during off-peak hours) will require significant reflection with the energy managers.

The metamodel is not only much faster than TRNSYS, it is also more flexible in its usage and manipulation.
For instance, we could modify our model to provide uncertainty estimation, using the methods developed in Chapter~\ref{chap:uncertainty-modelling} and Chapter~\ref{chap:latent-distributions}.
Although it would bring a valuable insight regarding the chosen optimization scenario, it is still unclear how to conjugate uncertainty estimation with the calibration and optimization tasks.
We leave this question open for future works.
Additionally, the metamodel allows for different input parameters at each time step of the simulation.
Instead of using the NSGA-II algorithms to solve our optimization task, we could explore reinforcement learning methods, and assign a reward associated with each hour to hour policy. This would be computationally intensive but reinforcement learning approaches allow to define new user-specific and non differentiable rewards which could benefit from insights from energy managers.

\paragraph{Sequential Monte Carlo methods. }
In order to demonstrate the use cases for SMC methods in our decoupled architecture, we utilised simple smoothing algorithms, known to quickly degenerate when dealing with longer time series.
Many alternative to the Path-space smoother have already been proposed in the literature, that allow to mitigate particle degeneracy with limited additional cost.
Implementing such algorithms seems like the natural next step in the process of improving our methodology.

In addition, SMC methods shine in that parameters can be updated online, a use case particularly adapted to Oze-Energies.
Modelling the changes in a building during an entire is extremely complex, which is why we usually limit ourselves to short time periods, about a few months.
Instead of performing independent trainings for different part of the year, we could update the learnt parameters all along the year, in order to better match local weather and occupation conditions.

\paragraph{Variational discrete latent models. }
In this thesis, we presented simple Variational Inference training procedures, in order to focus on the models and their applications.
In the future, it would be interesting to develop parameter estimations with more complex methods from the literature, such as the Importance Weighted VAE which allows to tighten the lower bound on the likelihood, or the $\beta$-VAE which introduced an new hyper parameter for balancing the prior loss term with reconstruction accuracy.

Additionally, one could explore more diverse sampling procedures and prior models.
In the presented experiments, we constrained ourselves to quantizing vectors by sampling one of the neighboring codebook.
Yet our framework is designed for any discrete law, which opens new perspectives for discrete latent models.
Similarly, comparing the performance of more diverse prior models could bring new insights toward their impact on the overall performance, as well as on the modelled distribution of the latent states.

Finally, we believe these architectures could be relevant in semi supervised learning, for instance by first training the generative model unsupervised, then using the available annotations to finetune the prior model.
This way, we would be able to leverage important amounts of data, with limited annotation cost.
