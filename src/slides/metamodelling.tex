\section{Metamodelling}

\subsection{Training}
\begin{frame}
	\frametitle{Dataset sampling}
	We sample \alert{various combination} of input variables, and store the associated simulations from TRNSYS.
	The dataset created contains over 100,000 samples to cover every \alert{possible scenario} as best as possible.
	\begin{figure}[htpb]
		\centering
		\includegraphics[width=0.5\textwidth]{images/metamodel/dataset_sampling.png}
	\end{figure}
\end{frame}

\begin{frame}
	\frametitle{Sequential architectures benchmarked}
	\setbeamercovered{invisible}
	\begin{figure}[htpb]
		\centering
		\includegraphics[width=0.35\textwidth]{images/metamodel/rnn.png}
		\pause
		\includegraphics[width=0.35\textwidth]{images/metamodel/LSTM_cell.png}
		\caption{Our metamodel architecture (left), and a detailed LSTM cell (right). The LSTM cell improves on the classic RNN by introducing a cell state $c_t$ supposed to carry long term memory, without additional alterations, throughout the sequence.}
	\end{figure}
\end{frame}

\begin{frame}
	\frametitle{LSTM equations}
	\begin{definition}[LSTM Cell]
		At each layer $\ell$ and each time step $t$, the LSTM Cell can be computed as follows:
		\begin{align*}
			\Gamma_i^\ell & = \sigma(W_{xi}^\ell u_t^\ell + W_{hi}^\ell h_{t-1}^\ell + b_i^\ell)\,, \\
			\Gamma_f^\ell & = \sigma(W_{xf}^\ell u_t^\ell + W_{hf}^\ell h_{t-1}^\ell + b_f^\ell)\,, \\
			\Gamma_o^\ell & = \sigma(W_{xo}^\ell u_t^\ell + W_{ho}^\ell h_{t-1}^\ell + b_o^\ell)\,, \\
			\tilde c_t    & = \tanh(W_{xc}^\ell u_t^\ell + W_{hc}^\ell h_{t-1}^\ell + b_c^\ell)\,,  \\
			c_t^\ell      & = \Gamma_f^\ell * c_{t-1}^\ell + \Gamma_i^\ell * \tilde c_t\,,          \\
			h_t^\ell      & = \Gamma_o^\ell * \tanh c_t^\ell\,.
		\end{align*}
		\footnotesize{Where $\theta = \left\{\left(W_{xi}^\ell,W_{hi}^\ell,W_{xf}^\ell,W_{hf}^\ell,W_{xo}^\ell,W_{ho}^\ell,W_{xc}^\ell,W_{hc}^\ell,W_y,b_{i}^\ell,b_{f}^\ell,b_{o}^\ell,b_{c}^\ell,b_y\right)_{1\leq \ell \leq L}\right\}$ are the weights vector and matrices of the model.}
	\end{definition}
\end{frame}

\begin{frame}
	\frametitle{Parameters estimation}
	Parameters of each model are estimated through \alert{gradient descent}, using the Mean Squared Error (MSE) loss and the Adam optimizer \cite{Kingma2015AdamAM}.
	\begin{algorithm}[H]
		\caption{Gradient descent}
		\begin{algorithmic}
			\For{each sample $(u^{(i)}, y^{(i)})$}
			\State $\hat y \gets f^\theta_\text{building}(u^{(i)})$
			\State $E \gets \mathcal L (y^{(i)}, \hat y)$
			\State $\theta \gets \theta - \alpha \partial E / \partial \theta$
			\EndFor
		\end{algorithmic}
	\end{algorithm}
\end{frame}

\begin{frame}
	\frametitle{Results}
	\begin{table}
		\caption{Metrics of the benchmarked models on the validation splits. The best mean values are displayed in bold.}
		\centering
		\resizebox{\textwidth}{!}{
			\begin{tabular}{ cccccc }
				\hline
				                                                & BiGRU           & Transformer     & LSTM                      & ConvGru          & FFN             \\
				\hline
				$\mathrm{Loss}$ ($\times 10^{-4}$)              & $2.05 \pm 1.61$ & $2.72 \pm 2.88$ & $\textbf{1.65}  \pm 1.47$ & $2.26 \pm 2.04$  & $90.5 \pm 41.1$ \\
				$\mathrm{MSE_T}$ \;\;($\times 10^{-5}$)         & $1.00 \pm 1.53$ & $1.49 \pm 2.75$ & $\textbf{0.82}  \pm 2.05$ & $1.20 \pm 2.05 $ & $39.5 \pm 39.7$ \\
				$\mathrm{MSE_Q}$ \;\;($\times 10^{-4}$)         & $3.84 \pm 2.05$ & $4.12 \pm 3.01$ & $\textbf{2.75}  \pm 1.72$ & $3.53 \pm 1.77$  & $172  \pm 60.2$ \\
				$\mathrm{MSE_T^{occ}}$ ($\times 10^{-5}$)       & $4.60 \pm 7.16$ & $6.56 \pm 12.2$ & $\textbf{3.79}  \pm 6.81$ & $5.89 \pm 9.91$  & $176  \pm 189$  \\
				$\mathrm{MSE_Q^{occ}}$  ($\times 10^{-4}$)      & $1.45 \pm 1.00$ & $2.07 \pm 1.80$ & $\textbf{1.22} \pm 0.878$ & $1.85 \pm 1.23$  & $113  \pm 50.8$ \\
				$\Delta_{Q^{\mathrm{Tot}}}$  ($\times 10^{-3}$) & $4.03 \pm 11.7$ & $20.1 \pm 12.4$ & $\textbf{2.46} \pm 12.0$  & $14.9 \pm 16.2$  & $182 \pm 68.8$  \\
				$\mathrm{Time}\;\;\;\;\; (\times 10^{-2}s)$     & 6.46            & 4.52            & 6.51                      & 6.77             & \textbf{0.341}  \\
				\hline
			\end{tabular}}
	\end{table}
\end{frame}

\subsection{Calibration}
\begin{frame}
	\frametitle{Criteria}
	We minimize the following criteria using the \alert{CMA-ES} optimization algorithm to reach an estimate of the building's properties.
	\begin{block}{Calibration criteria (temperature)}
		\begin{align*}
			\mathrm{MBE}_T (\%)      & = 100\frac{\sum_{t=1}^T{\left(\tau_t - \hat \tau_t\right)}}{\sum_{t=1}^T{\tau_t}} \quad         \\
			Cv(\mathrm{RMSE})_T (\%) & = \frac{100}{\bar \tau} \left( \frac{\sum_{t=1}^T{(\tau_t-\widehat{\tau}_t)^2}}{T}\right)^{1/2}
		\end{align*}
	\end{block}
\end{frame}

\begin{frame}
	\frametitle{Results - Convergence}
	\begin{figure}[htpb]
		\centering
		\includegraphics[width=\textwidth]{images/calib-optim/calib_hist.jpg}
		\caption{Calibration cost evolution for the metamodel and TRNSYS, on Livingstone and Stanley. Both models were calibrated for 300 epochs, which is enough to reach convergence for Stanley, but not Livingstone.}
	\end{figure}
\end{frame}

\begin{frame}
	\frametitle{Results - Metrics}
	\begin{table}[htpb]
		\centering
		\caption{The metamodel and TRNSYS \alert{perform similarly} when calibrated for the same number of iterations.
			Only the metamodel is able to reach convergence for Livingstone in a \alert{reasonable time frame}.}
		\resizebox{\textwidth}{!}{
			\begin{tabular}{*7c}
				                  & $\mathrm{MBE}_Q$ & $Cv(\mathrm{RMSE})_Q$ & $\mathrm{MBE}_T$ & $Cv(\mathrm{RMSE})_T$ & Iterations & Computation time \\
				\hline
				{\bf Stanley}     &                  &                       &                  &                       &            &                  \\
				Metamodel         & -0.627           & {\bf 11.0}            & 0.134            & {\bf 1.20}            & 300        & 2mn              \\
				TRNSYS            & -0.409           & 12.1                  & -0.264           & 1.24                  & 300        & 3h               \\
				\hline
				{\bf Livingstone} &                  &                       &                  &                       &            &                  \\
				Metamodel         & -0.690           & {\bf 14.2}            & -0.0551          & {\bf 1.29}            & 10000      & 1h               \\
				Metamodel         & -0.574           & {\bf 14.2}            & -0.413           & 1.95                  & 300        & 2mn              \\
				TRNSYS            & -1.08            & 15.8                  & 0.156            & 1.96                  & 300        & 3h               \\
			\end{tabular}
		}
	\end{table}
\end{frame}

\begin{frame}
	\frametitle{Results - Visuals}
	\begin{figure}[htpb]
		\centering
		\caption{Consumption and temperature simulations after calibration, for both the metamodel and TRNSYS, for Stanley.}
		\includegraphics[width=0.7\textwidth]{images/calib-optim/calib_week2_t_int.png}
		\includegraphics[width=0.7\textwidth]{images/calib-optim/calib_week2_private.png}
	\end{figure}
\end{frame}

\subsection{Optimization}
\begin{frame}
	\frametitle{Criteria}
	Compared to the calibration:
	\begin{itemize}
		\item We estimate the \alert{usages} instead of the building's properties.
		\item We do not aim to approximate real data, but \alert{improve energy consumption} for the building.
		\item We use the NSGA-II for \alert{multi objective} optimization.
	\end{itemize}
	\begin{block}{Optimization criteria}
		\begin{align*}
			\mathcal{L}^\text{optim}_\text{\textcolor{blue}{temperature}}(T^\star, \tau) & = \sum_{t \in T_\mathrm{Occ}} (T^\star - \tau_t)^2\,, \\
			\mathcal{L}^\text{optim}_\text{\textcolor{orange}{energy}}(\zeta)            & = \sum_{t=1}^T \zeta_t\,,
		\end{align*}
	\end{block}
\end{frame}

\begin{frame}
	\frametitle{Results - Pareto}
	\begin{figure}[htpb]
		\centering
		\caption{Pareto front after optimization for the Stanley (left) and Livingstone (right) building. We select the point of closest equivalent comfort, corresponding to a 5.3\% (Stanley) and 9.9\% (Livingstone) reduction in consumption.}
		\includegraphics[width=0.25\textwidth]{images/calib-optim/pareto_stanley.png}
		\includegraphics[width=0.25\textwidth]{images/calib-optim/pareto_livingstone.png}
		\label{fig:pareto}
	\end{figure}
\end{frame}

\begin{frame}
	\frametitle{Results - Metrics}
	\begin{table}[htpb]
		\centering
		\label{tab:optim}
		\resizebox{\textwidth}{!}{
			\begin{tabular}{c|cc|cc}
				                               & relative gain (\%) & prevision (MWh) \\
				\hline
				Stanley                        & 5.32               & 8.05            \\
				Stanley $\pm 0.5^\circ\!$C     & 10.5               & 15.9            \\
				Livingstone                    & 9.92               & 9.96            \\
				Livingstone $\pm 0.5^\circ\!$C & 17.3               & 17.3
			\end{tabular}
		}
		\caption{Energy load reduction between calibration and optimization steps, when maintaining the initial level of comfort.
			Our end-to-end methodology is described in \cite{Cohen2021metamodelling}.}
	\end{table}
\end{frame}

\begin{frame}
	\frametitle{Results - Visuals}
	\begin{figure}[htpb]
		\centering
		\caption{Consumption and temperature simulations after optimization (metamodel) for the Livingstone building.}
		\includegraphics[width=0.7\textwidth]{images/calib-optim/optim_week2_t_int.png}
		\includegraphics[width=0.7\textwidth]{images/calib-optim/optim_week2_private.png}
	\end{figure}
\end{frame}

\begin{frame}
	\frametitle{Was the metamodel useful ?}
	Until now:
	\begin{itemize}
		\item The metamodel is \textit{only} much faster ($\times 70$).
	\end{itemize}
	\pause
	From now on, we can:
	\begin{itemize}
		\item Infer knowledge from the data gathered.
		      \begin{itemize}
			      \item We now explore \alert{uncertainty estimation}.
		      \end{itemize}
		      \pause
		\item Develop gradient based calibration and optimization for high dimensional spaces.
		\item Estimate hour by hour optimization policies - through Reinforcement Learning for instance.
	\end{itemize}
\end{frame}
