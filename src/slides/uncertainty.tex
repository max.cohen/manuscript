\section{Uncertainty estimation}

\subsection{Decoupled learning}
\begin{frame}
	\frametitle{Air quality analysis}
	\setbeamercovered{invisible}
	Air quality depends on multiple factors such as \coo, indoor temperature, etc. We now focus on the \alert{indoor relative humidity}.
	Can we model the \textcolor{blue}{uncertainty} of our predictions from historic data ?
	\pause
	\begin{figure}
		\centering
		\caption{Sterling Chart introduced in \cite{Sterling1985Chart}.}
		\includegraphics[width=0.45\textwidth]{images/airq/sterling-chart.jpg}
	\end{figure}
\end{frame}

\begin{frame}
	\frametitle{Relative humidity dataset}
	\begin{figure}
		\centering
		\includegraphics[width=0.65\textwidth]{images/airq/air_quality_dataset.png}
	\end{figure}
\end{frame}

\begin{frame}
	\frametitle{Traditional approach - HMM}
	Consider the following Hidden Markov Model: at each time step $t$, the observations $Y_t$ depends on a discrete hidden state $Z_t \in \{1, \cdots, K\}$, with $Y_t | Z_t = k \sim \mathcal{N} (\mu_k, \sigma_k^2)$ and transition matrix $A$ such that $p(Z_t = k | Z_{t-1} = j) = a_{kj}$.
	\pause
	\begin{block}{Log likelihood}
		In this context, the log likelihood can be computed explicitly:
		\footnotesize{\begin{align*}
				\ell(y_{1:T}, z_{1:T} ; \theta) & = \sum_{t=1}^T \sum_{k=1}^K \log p(y_t; \mu_k, \sigma_k) \mathds{1}\{z_t=k\}               \\
				                                & + \sum_{t=1}^T \sum_{k=1}^K \sum_{j=1}^K \log(a_{kj}) \mathds{1} \{z_t = j, z_{t-1} = k \} \\
				                                & + \sum_{k=1}^K \log \pi_k \mathds{1} \{z_0 = k\}
			\end{align*}}
	\end{block}
\end{frame}

\begin{frame}
	\frametitle{Neural network model}
	We experiment with a \alert{simple LSTM model}, trained by gradient descent.
	\begin{figure}[htpb]
		\centering
		\includegraphics[width=0.5\linewidth]{images/airq/lstm/oze_4.png}
		\includegraphics[width=0.5\linewidth]{images/airq/lstm/oze_1.png}
	\end{figure}
\end{frame}

\begin{frame}
	\frametitle{A compromise between bias and uncertainty}
	\begin{table}
		\centering
		\begin{tabular}{llll}
			\toprule
			     & RMSE                     \\
			\toprule
			LSTM & $\mathbf{0.31 \pm 0.18}$ \\
			HMM  & $0.46 \pm 0.19$          \\
			\bottomrule
		\end{tabular}
	\end{table}
	Conclusion:
	\begin{itemize}
		\item The LSTM has a \textcolor{teal}{low bias} but \textcolor{red}{no uncertainty estimation}.
		\item The HMM has a \textcolor{red}{high bias} but can perform \textcolor{teal}{uncertainty quantification}.
	\end{itemize}
	We want the best of both worlds !
\end{frame}

\begin{frame}
	\frametitle{Decoupled learning}
	\setbeamercovered{invisible}
	We propose to decouple representation learning and uncertainty estimation.
	By leveraging Neural Network's ability to extract \alert{high level features} \cite{Hinton2006ReducingDimensionality}, we can fit a HMM in a in a \alert{low dimensional space}.
	\pause
	\begin{figure}[htpb]
		\centering
		\includegraphics[width=0.5\linewidth]{images/airq/auto_encoder_archi.png}
		\label{fig:lastlayer_autoencoder}
	\end{figure}
\end{frame}

\subsection{Sequential Monte Carlo}
\begin{frame}
	\frametitle{Proposed model}
	\setbeamercovered{invisible}
	\vspace*{-0.5cm}
	\begin{align*}
		\widetilde u_{1:T} & = h_\phi(u_{1:T})\,,                            & \text{input model,}       \\
		x_t                & = g_\theta(x_{t-1}, \widetilde u_t) + \eta_t\,, & \text{state model,}       \\
		y_t                & = f_\theta(x_t) + \varepsilon_t\,,              & \text{observation model.}
	\end{align*}
	% where $\theta$ are the unknown real-valued parameters of the network and $f_\theta$ and $g_\theta$ are nonlinear parametric functions, and $(\eta_t)_{t\geq 1}$ and $(\varepsilon_t)_{t\geq 1}$ are two independent sequences of i.i.d. centered Gaussian random variables.
	\pause
	\begin{figure}
		\centering
		\includegraphics[width=0.4\linewidth]{images/airq/lastlayer/architecture.png}
	\end{figure}
\end{frame}

\begin{frame}
	\frametitle{Gradient based estimation}
	The likelihood of the observations is not available explicitly, as it would require integrating over the distribution of the hidden states $x_{1:T}$:
	$$
		L : \theta \mapsto \int p_\theta(y_{1:T}, x_{1:T}) \mathrm d x_{1:T},
	$$
	\pause
	Consequently, the score function $\theta \mapsto \nabla_\theta \log L(\theta)$ is intractable.
	We propose to optimize instead a Monte Carlo estimator using Fisher's identity:
	$$
		\nabla_\theta \log p_\theta(y_{1:T}) = \mathbb{E}_\theta \left[ \nabla_\theta\log p_\theta(x_{1:T}, y_{1:T}) | y_{1:T} \right]
	$$
\end{frame}

\begin{frame}
	\frametitle{Particle filter}
	We iteratively approximate the filtering distributions $p_\theta(x_{t} | y_{1:t})$, for $t \leq T$, by a set of $N$ particles $(\xi^{\ell}_t)_{1 \leq t \leq T, 1 \leq \ell \leq N}$.
	\pause
	At $t = 0$, $(\xi^{\ell}_0)_{\ell=1}^N$ are sampled independently from $\rho_0 = \Psi_{0, \Sigma_x}$, and each particle $\xi^{\ell}_0$ is associated with the standard importance sampling weight:
	$$
		\omega_0^{\ell} \propto  \Psi_{Y_0, \Sigma_y}(f_\theta(\xi^{\ell}_0)) \,.
	$$
	\pause
	Then, for $t\geq 1$, using $\{(\xi^{\ell}_{t-1},\omega^{\ell}_{t-1})\}_{\ell=1}^N$,  pairs $\{(I^{\ell}_t,\xi^{\ell}_t)\}_{\ell=1}^N$ of indices and particles are sampled from the instrumental distribution:
	$$
		\pi_t(\ell,x) \propto \omega_{t-1}^{\ell} p_t(\xi^{\ell}_{t-1}, x)\,.
	$$
	% In this application we use for $p_t(\xi^{\ell}_{t-1}, \cdot)$ the prior kernel $\Psi_{g_\theta(\xi^\ell_{t-1}, \tilde u_t), \Sigma_x}$.
	$\xi^{\ell}_t$ are associated with the importance weight $\omega^{\ell}_t \propto \Psi_{Y_t, \Sigma_y}(f_\theta(\xi^{\ell}_t))$.
\end{frame}

\begin{frame}
	\frametitle{Particle smoother}
	Then, the smoothing distribution $p_\theta(x_{1:T}|y_{1:T})$ can be approximated by associating importance weights $(\omega^{\ell}_T)_{\ell=1}^N$ with each particle genealogy $(\xi^{\ell}_{1:T})_{\ell=1}^N$.
	\pause
	\begin{block}{Gradient estimation}
		The score function can now be approximated by:
		$$
			\widehat {S}^N_\theta(y_{1:T}) = \sum_{\ell=1}^N \omega_T^\ell\nabla_\theta\log p_\theta(\xi^\ell_{1:T}, y_{1:T})\,,
		$$
	\end{block}
\end{frame}

\begin{frame}
	\frametitle{Example: the Path-space smoother}
	For the Path-space smoother, the genealogical trajectories are defined recursively and updated at each time step with the particles and indices $(\xi^\ell_{t+1}, I^\ell_{t+1})$: for all $0 \leq t \leq T - 1$,
	$$\xi^\ell_{0:t+1} = \left(\xi^{I^\ell_{t+1}}_{0:t}, \xi^\ell_{t+1}\right)$$
	\begin{figure}[htpb]
		\centering
		\includegraphics[width=0.5\linewidth]{images/airq/lastlayer/particle_filter.png}
	\end{figure}
\end{frame}

\begin{frame}
	\frametitle{Qualitative benchmark}
	\begin{figure}[htpb]
		\centering
		\includegraphics[width=0.55\linewidth]{images/airq/lastlayer/ci_rh_1.png}
		\includegraphics[width=0.55\linewidth]{images/airq/lastlayer/ci_rh_2.png}
	\end{figure}
\end{frame}

\begin{frame}
	\frametitle{Quantitative results}
	Predictions can be performed by approximating the predictive density $p_{\theta,\phi}(y_{t+1}|u_{1:t+1},y_{1:t})$ by:
	$$
		p^N_{\widehat\theta,\widehat\phi}(y_{t+1})= \sum_{i=1}^{N}\omega_t^i p_{\widehat\theta,\widehat\phi}(y_{t+1}|\xi_t^i,u_{t+1})\,.
	$$
	\begin{table}[htpb]
		\centering
		\begin{tabular}{llll}
			\toprule
			            & RMSE                       \\
			\toprule
			SMCL (ours) & $0.30 \pm 0.19$          & \\
			MC Dropout  & $\mathbf{0.29 \pm 0.18}$ & \\
			\toprule
			LSTM        & $0.31 \pm 0.18$          & \\
			HMM         & $0.46 \pm 0.19$          & \\
			\bottomrule
		\end{tabular}
	\end{table}
\end{frame}


\subsection{Variational Inference}
\begin{frame}
	\frametitle{Variational Inference - Introduction}
	Variational Inference aims at reaching a compromise between the expressivity and complexity of the approximate posterior function.
	\begin{figure}[htpb]
		\centering
		\includegraphics[width=0.8\textwidth]{images/airq/vadiltis/vi_introduction.png}
	\end{figure}
\end{frame}

\begin{frame}
	\frametitle{Variation Inference - Definition}
	Suppose that the \textcolor{blue}{observations $y$} depend on a \textcolor{orange}{latent variable $z$}, and let $\{ q_\phi, \phi \in \Phi \}$ be a family of tractable distributions.
	We can define a lower bound on the likelihood called the \textcolor{violet}{Evidence Lower BOund (ELBO) $\elbo_{\theta, \phi}(y)$}:
	\begin{align*}
		\log p_\theta(y) & = \log \expectation \left[ \frac{p_\theta(y, z)}{q_\phi(z | y)} \right]                                                                \\
		                 & \leq \expectation \left[ \log \frac{p_\theta(y|z) p_\theta(z)}{q_\phi(z | y)} \right] = \textcolor{violet}{\elbo_{\theta, \phi}(y)}\,.
	\end{align*}
\end{frame}

\begin{frame}
	\frametitle{Proposed model}
	We consider that the observations $(y_t)_{t=1}^T$ depend on \textcolor{orange}{discrete latents $(\quantized_t)_{t=1}^T$} following a Markov chain.
	We chose the following family of posterior functions:
	\begin{align*}
		q_\phi(\quantized_t = k | y_{1:T})  = \softmax & ( \{- \| \encoding^t - l \|_2^2\}_{1 \leq l \leq K} )_k \\
		\propto \exp                                   & ( \{ - \| \encoding^t - k \|_2^2 \} )\,,
	\end{align*}
	where \textcolor{blue}{$\encoding$} $ = f_\phi(y_{1:T})$ is an encoding of the observation.
	\begin{figure}
		\centering
		\includegraphics[width=0.4\linewidth]{images/airq/vadiltis/archi.png}
	\end{figure}
\end{frame}

% \begin{frame}
% 	\frametitle{Gumbel Softmax: Reparametrization for categorical variables}
% 	Equations for drawing from the GS and evolving histograms from the paper.
% \end{frame}

\begin{frame}
	\frametitle{Qualitative benchmark}
	\begin{figure}[htpb]
		\centering
		\includegraphics[width=0.55\linewidth]{images/airq/vadiltis/ci_oze_1.png}
		\includegraphics[width=0.55\linewidth]{images/airq/vadiltis/ci_oze_2.png}
		\label{fig:vadiltis_ci_rhdataset}
	\end{figure}
\end{frame}

\begin{frame}
	\frametitle{Quantitative results}
	\begin{table}[htpb]
		\centering
		\begin{tabular}{llll}
			\toprule
			           & RMSE                       \\
			\toprule
			Ours (gru) & $\mathbf{0.30 \pm 0.19}$ & \\
			Ours (rnn) & $\mathbf{0.30 \pm 0.20}$ & \\
			Ours (cnn) & $0.31 \pm 0.20$          & \\
			VQ-VAE     & $0.55 \pm 0.33$          & \\
			\toprule
			SMCL       & $0.30 \pm 0.19$          & \\
			\toprule
			LSTM       & $0.31 \pm 0.18$          & \\
			HMM        & $0.46 \pm 0.19$          & \\
			\bottomrule
		\end{tabular}
		\caption{See Table~4.4 in the manuscript for more information.
			We also proposed much more \alert{complex prior models}, see \cite{Cohen2022diffusionbridges}.}
	\end{table}
\end{frame}

\begin{frame}
	\frametitle{Codebook usage}
	\begin{figure}
		\centering
		\caption{We sampled $N=100$ trajectories from the prior model, and plotted the associated codebook index at each time step.
			Some of the codebooks appear to be linked to particular behaviors of the building, for instance codebook 6 matches an increase of relative humidity at the beginning of  the day.}
		\includegraphics[width=0.65\textwidth]{images/airq/vadiltis/codebook_usage.png}
	\end{figure}
\end{frame}
