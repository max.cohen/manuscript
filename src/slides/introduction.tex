% We will talk about modelling dynamic systems through Bayesian approaches.
% All along this presentation, our use case and main example is building management.
% This is thanks to the problematic of Oze - now Accenta.
\section{Introduction}

% And so let's start by talking about building management at Accenta.
\subsection{Industrial context}

% Picture yourself - in a big building. There are hundred, maybe thousands people working here with you. We are looking at modelling the energetic and thermal behavior of this building, including its occupants, the diverse machinery - such as computers, lights, maybe fridges - and its energy consumption.
% Note that we are talking about energy and not electricity !
\begin{frame}
	\frametitle{Building management introduction}
	\begin{figure}
		\centering
		\includegraphics[width=5cm]{images/oze/bat.jpg}
		\includegraphics[width=5cm]{images/oze/batschem.jpg}
		\caption{A typical building and its schematics.}
	\end{figure}
\end{frame}

% Stress again energy and not electricity
\begin{frame}
	\frametitle{French consumption graph}
	\begin{figure}[ht]
		\centering
		\includegraphics[width=0.8\textwidth]{images/oze/repartition_consommation_france.png}
		\caption{French energy consumption in 2020 \footnote{\tiny{www.statistiques.developpement-durable.gouv.fr}}}
		\label{fig:oze:france_consumption}
	\end{figure}
\end{frame}

\begin{frame}
	\frametitle{Oze Energies}
	\begin{figure}[htpb]
		\centering
		\includegraphics[width=0.6\textwidth]{images/oze/Oze presentation.jpg}
	\end{figure}
\end{frame}


% Introduce Accenta, new projects and how Oze comes into it. Closes the introduction of the introduction.
\begin{frame}
	\frametitle{Accenta}
	\begin{figure}[htpb]
		\centering
		\includegraphics[width=5cm]{images/oze/accenta_logo.png}
	\end{figure}
	\begin{figure}
		\centering
		\includegraphics[width=0.6\textwidth]{images/oze/accenta.png}
	\end{figure}
\end{frame}

\subsection{Data}
% At this point, we start studying the problem, and we begin with the data.
\begin{frame}
	\frametitle{Observed signals}
	Observed signals we aim to model:
	\begin{itemize}
		\item Indoor temperature is denoted $(\tau_t)_{t=1}^T$.
		\item Consumption is denoted $(\zeta_t)_{t=1}^T$.
	\end{itemize}
	\begin{figure}[htpb]
		\centering
		\includegraphics[width=0.85\textwidth]{images/oze/observed_signals.png}
	\end{figure}
\end{frame}

\begin{frame}
	\frametitle{Input variables}
	The input variables are:
	\begin{itemize}
		\item Building properties $\lambda$ and occupation $(\delta_t)_{t=1}^T$;
		\item Usage settings $(\psi_t)_{t=1}^T$;
		\item Weather data is denoted $(\varphi_t)_{t=1}^T$.
	\end{itemize}
	\begin{figure}[htpb]
		\centering
		\includegraphics[width=0.7\textwidth]{images/oze/input_variables.png}
	\end{figure}
\end{frame}


\begin{frame}
	\frametitle{Building simulators}
	\begin{definition}[Building simulator]
		Building simulators such as TRNSYS simulate the evolution of indoor temperature and consumption of buildings specified by the input variables.
		$$f_\text{building}: (\lambda, \psi, \delta, \varphi) \mapsto (\hat \tau, \hat \zeta).$$
	\end{definition}
	\begin{figure}[htpb]
		\centering
		\includegraphics[width=0.6\textwidth]{images/oze/trnsys.png}
	\end{figure}
\end{frame}

\subsection{Problematic}
\begin{frame}
	\frametitle{Calibration}
	\setbeamercovered{invisible}
	\begin{definition}[Calibration]
		The calibration aims at \alert{estimating the properties $\lambda$} of the building, by minimizing:
		$$
			\mathcal{L}_{\text{calib}}(\lambda) = \mathcal{L}^\text{calib}_\text{temperature}(\tau, \hat \tau) + \beta \mathcal{L}^{\text{calib}}_{\text{energy}}(\zeta, \hat \zeta)
		$$
	\end{definition}
	\pause
	\begin{figure}[htpb]
		\centering
		\includegraphics[width=0.7\textwidth]{images/oze/calibration.png}
	\end{figure}
\end{frame}

\begin{frame}
	\frametitle{Optimization}
	\setbeamercovered{invisible}
	\begin{definition}[Optimization]
		The optimization determines a \alert{usage parameters $\psi$} corresponding to a \alert{compromise} between consumption and comfort.
		\begin{equation*}
			\mathcal{L}^\text{optim}_\text{\textcolor{blue}{temperature}}(T^\star, \hat \tau) \quad \text{and} \quad \mathcal{L}^\text{optim}_\text{\textcolor{orange}{energy}} (\hat \zeta)\,,
		\end{equation*}
		where $T^\star$ is the target temperature.
	\end{definition}
	\pause
	\begin{figure}[htpb]
		\centering
		\includegraphics[width=0.7\textwidth]{images/oze/optimization.png}
	\end{figure}
\end{frame}

\begin{frame}
	\frametitle{Pareto front}
	\begin{definition}[Pareto front]
		A feasible solution $\psi^\star \in \Psi$ is said to Pareto dominate another solution $\psi \in \Psi$ if both:
		\begin{enumerate}
			\item $\mathcal{L}^\text{optim}_\text{\textcolor{blue}{temperature}}(\psi^\star) \leq \mathcal{L}^\text{optim}_\text{\textcolor{blue}{temperature}}(\psi)\; \text{and}\; \mathcal{L}^\text{optim}_\text{\textcolor{orange}{energy}}(\psi^\star) \leq \mathcal{L}^\text{optim}_\text{\textcolor{orange}{energy}}(\psi)$,
			\item $\mathcal{L}^\text{optim}_\text{\textcolor{blue}{temperature}}(\psi^\star) > \mathcal{L}^\text{optim}_\text{\textcolor{blue}{temperature}}(\psi)\; \text{or }\; \mathcal{L}^\text{optim}_\text{\textcolor{orange}{energy}}(\psi^\star) > \mathcal{L}^\text{optim}_\text{\textcolor{orange}{energy}}(\psi)$.
		\end{enumerate}
		A solution $\psi^\star \in \Psi$ is then called Pareto optimal if there does not exist another solution that dominates it.
		The set of Pareto optimal outcomes, is called the Pareto front.
	\end{definition}
\end{frame}

\begin{frame}
	\frametitle{Pareto front example}
	\begin{columns}[c]
		\begin{column}{0.5\textwidth}
			\begin{figure}[htpb]
				\centering
				\includegraphics[width=0.7\textwidth]{images/calib-optim/pareto_livingstone.png}
			\end{figure}
		\end{column}
		\begin{column}{0.45\textwidth} % Right column width
			\begin{itemize}
				\item Points above are suboptimal
				\item Points below are unreachable
				\item Points on the front are optimal compromises
			\end{itemize}
		\end{column}
	\end{columns}
\end{frame}


\subsection{Objectives}
\begin{frame}
	\frametitle{Metamodelling TRNSYS}
	We want to develop a metamodel that:
	\begin{itemize}
		\item Is \alert{faster} than TRNSYS
		\item Can infer knowledge from \alert{real data}
	\end{itemize}
	\begin{figure}[htpb]
		\centering
		\includegraphics[width=0.6\textwidth]{images/metamodel/metamodel_training.png}
	\end{figure}
\end{frame}

\begin{frame}
	\frametitle{Uncertainty quantification}
	By leveraging historic data of buildings, we want to model the uncertainty of our simulator.
	\begin{figure}[htpb]
		\centering
		\includegraphics[width=0.7\textwidth]{images/airq/lastlayer/ci_rh_1.png}
		\caption{Confidence intervals produced by one of our proposed approach.}
	\end{figure}
\end{frame}
