\chapter{Introduction (version française)}
\section{Oze-Energies}
Oze-Energies, nouvellement Accenta, est une entreprise française spécialisée dans l'optimisation de la consommation énergétique des bâtiments.
Alors que la demande énergétique en chauffage, climatisation et ventilation n'a cessé d'augmenter depuis plusieurs décennies, réduire l'impact environnemental du parc immobilier, tout en maintenant une qualité de confort raisonnable, reste un problème complexe.
Au travers de méthodes innovantes et durables, Oze-Energies vise à améliorer la qualité de l'air et le confort dans les bâtiments, tout en réduisant leur consommation, sans rénovation.

En 2009, le parc immobilier était responsable de 40\% de la consommation énergétique française, et près d'un quart des émissions de gaz à effet de serre (Loi Grenelle\footnote{\href{https://www.legifrance.gouv.fr/loda/id/JORFTEXT000020949548/2020-09-21/}{Loi Grenelle I, Article 3}}, Figure~\ref{fig:oze:france_consumption}).
La stratégie la plus directe pour réduire cette consommation consiste à rénover l'isolation des vieux bâtiments, ce qui mène généralement à des travaux longs et coûteux.
Malgré s'être fixé comme objectif de rénover 500 000 bâtiments par an avant 2020\footnote{\href{https://www.ecologie.gouv.fr/strategie-nationale-bas-carbone-snbc}{Stratégie Nationale Bas-Carbone (SNBC)}}\footnote{\href{https://www.legifrance.gouv.fr/loda/id/JORFTEXT000020949548/2020-09-21/}{Loi Grenelle I, Article 5}}, les résultats ne sont à ce jour pas satisfaisants, comme souligné par l'Ademe (Agence de la transition écologique)\footnote{\href{https://www.lefigaro.fr/conjoncture/accord-de-paris-pourquoi-les-pays-ne-sont-pas-a-la-hauteur-de-leurs-engagements-20190419}{Hervé Lefebvre, directeur du département Climat de l'Ademe, 2019}}.
Selon la SNBC (Stratégie Nationale Bas Carbone), le nombre moyen de rénovations annuelles effectuées d'ici 2030 avoisinera les 370 000 pour la période 2015-2030.
Oze-Energies a choisi une approche orthogonale en proposant aux gestionnaires de suivre des feuilles de route, produites sur mesure, afin de réduire la consommation de leur bâtiments.
Cela conduit à une réduction de 25\% en moyenne de la facture énergétique, sans nécessiter de travaux de rénovation.

La consommation énergétique d'un bâtiment peut être réduite en grande partie en jouant sur les paramètres de chauffage, ventilation et air conditionné (abrégé HVAC pour Heating Ventilation and Air Conditioning).
Oze-Energies travaille principalement sur l'occupation dans le secteur tertiaire où les comportements sont bien connus (journées classiques de travail, pas d'occupation le week-end, etc.), cependant on peut toujours noter des différences significatives d'un bâtiment à l'autre dues à une grande variété de facteurs.
On peut citer par exemple l'isolation, l'occupation du bâtiment ou encore l'utilisation de différents services de chauffage et climatisation.
Ces différences représentent une des difficultés techniques majeures de notre approche, que nous détaillons dans ce document.
Afin de modéliser précisément les échanges de chaleur de chaque bâtiment, Oze-Energies suit leur consommation et mesure la qualité de leur air de leur locaux en y installant des capteurs environnementaux, connectés à travers LoRA (un réseau public et sécurisé).
Ces données sont regroupées dans une base de données centralisée.
Les experts thermiciens de Oze-Energies peuvent alors les combiner avec leur connaissance de la thermique des bâtiments pour produire des feuilles de route qui détaillent un jeu de paramètres de HVAC permettant d'optimiser la consommation et d'assurer une bonne qualité de l'air.
Avec une réduction de la facture énergétique de 25\% en moyenne à confort constant, le coût de ce service est largement couvert par les économies réalisées : il s'agit du produit principal proposé par Oze-Energies, OPTIMZEN®.

Oze-Energies recherche des méthodes permettant de réduire le travail parfois répétitif de ses thermiciens: estimer les paramètres physiques des bâtiments, proposer de nouveaux scénarios de consommation, quantifier et comparer ces scénarios entre eux.
Une solution que nous étudierons dans les chapitres suivants consiste à utiliser un simulateur numérique, configuré pour estimer automatiquement le meilleur jeu de paramètres pour un bâtiment durant une période donnée, en simulant et comparant un grand nombre de politiques.
Ce logiciel expert, TRNSYS, permet de simuler les comportements complexes des bâtiments à partir d'une représentation schématisée, ainsi que de nombreux paramètres d'entrée comme l'occupation du bâtiment au cours de la période étudiée, les paramètres de HVAC ou encore les prévisions météorologiques.
TRNSYS est d'abord calibré pour représenter au mieux un bâtiment précis, à travers des méthodes de Machine Learning, en utilisant les données historiques du bâtiment récoltées par les capteurs.
Comme énoncé précédemment, cette tâche est d'autant plus complexe que la variété de sources de chaleur, de froid ou d'électricité augmente avec chaque nouveau client.
Une fois calibré, nous pouvons utiliser TRNSYS pour optimiser la consommation, tout en maintenant un niveau de confort et une qualité de l'air raisonnables.

Oze-Energies a été fondée en 2014, couvre plus de 5 millions de mètres carrés, étalés sur 500 bâtiments, et fait aujourd'hui partie de l'entreprise Accenta.

\begin{figure}[ht]
	\centering
	\caption{Consommation énergétique par principaux secteurs (France, 2020, www.statistiques.developpement-durable.gouv.fr)}
	\includegraphics[width=0.8\textwidth]{images/oze/repartition_consommation_france.png}
	\label{fig:oze:france_consumption}
\end{figure}

\section{Optimisation du confort et de la consommation}
\subsection{Capteurs, compteurs et données météorologiques}
\label{sub:sensors_counters}

Pour comprendre le comportement d'un bâtiment, nous combinons différents types de données, souvent récupérées directement sur le site.
La première étape de la production de feuilles de route consiste donc à installer des compteurs, qui transmettront aux bases de données de Oze-Energies à intervalles réguliers, en général toutes les 10 minutes.
Ils peuvent être rangés en trois catégories :
\begin{itemize}
	\item Les \textbf{capteurs} sont installés par les experts thermiciens à divers emplacements du bâtiment, et récupèrent des données ambiantes, telles que le niveau de \coo, l'humidité relative ou encore la température intérieure.
	      Parce que les données récoltées peuvent varier d'une zone du bâtiment à une autre, les capteurs présentent un problème d'agrégation complexe.
	\item Les \textbf{compteurs} enregistrent la consommation de chauffage, climatisation, ventilation et éventuellement d'électricité.
	      En général, nous n'avons accès qu'à une variable pour chaque source de consommation, il n'y a donc pas de tâche d'agrégation nécessaire.
	      La problématique principale liée aux compteurs réside dans la variété de fournisseurs d'énergie, de frigories et de calories : alors que certains nous transmettent une consommation horaire, d'autres ne nous donnent accès qu'à une donnée agrégée à la journée, ce qui implique un calcul de reconstruction non trivial.
	      De plus, la gestion des erreurs d'enregistrement ou de transmission peut aussi varier, certains fournisseurs ignorant les données aberrantes, alors que d'autres les compensent sur les signaux suivants.
	      Enfin, des fournisseurs différents peuvent répondre à un même besoin.
	      Par exemple, il est possible de chauffer à partir de chauffages électriques, ou bien de se connecter à un réseau de chaud.
	\item Les \textbf{données météorologiques}, telles que la température extérieure, l'humidité ou les niveaux d'irradiation solaire, ont un impact important sur le comportement d'un bâtiment.
	      Des données historiques correspondant à la zone étudiée sont disponibles pour les cinq dernières années.
	      Pour réaliser les feuilles de route, on peut aussi utiliser les prévisions météorologiques, qui sont considérées précises jusqu'à une semaine dans le futur.
	      Ces données ne sont pas récoltées directement par Oze-Energies, mais par Meteotest\footnote{https://meteotest.ch/}, une entreprise experte dans le domaine.
\end{itemize}
Un échantillon des données historiques est présenté dans la Figure~\ref{fig:chap1fr-sensors-counters-weather}.

\begin{figure}[htpb]
	\centering
	\caption{Nous avons tracé un échantillon d'une semaine de données historiques correspondant aux capteurs (température intérieure), compteurs (consommation) et données météorologiques (Direct Normal Irradiance).}
	\includegraphics[width=0.8\textwidth]{images/oze/sensors_counters_weather.png}
	\label{fig:chap1fr-sensors-counters-weather}
\end{figure}

\subsubsection{Simulation de l'évolution des bâtiments avec TRNSYS}
Les simulateurs physiques permettant de simuler l'évolution d'un bâtiment reposent généralement sur des équations de propagation thermique.
Ils permettent de prédire, en particulier, l'énergie consommée et la température intérieure à partir d'une description schématique du bâtiment, des matériaux de construction ainsi que de leur dimensions, des données météorologiques et des usages et paramètres de HVAC.
Due à la complexité de la tâche à résoudre, seule une poignée de simulateurs performants existent à ce jour.

Par exemple, EnergyPlus est utilisé dans \cite{shabunko2018energyplus} afin de définir trois types génériques de bâtiments, et ainsi de comparer les performances énergétiques de 400 bâtiments résidentiels.
Dans \cite{zhao2016occupant}, les auteurs proposent un framework de prédiction basé sur Matlab et EnergyPlus, qui leur permet d'optimiser la consommation d'énergie tout en respectant des préférences de confort individuelles.
Dans \cite{Magnier2010MultiobjectiveOO}, les auteurs mettent en avant les performances de TRNSYS comme simulateur physique, tout comme ses limites en termes de coût de calcul : selon les auteurs, un processus d'optimisation complet aurait pris jusqu'à 10 ans s'il n'avaient pas remplacé TRNSYS par un modèle substitut, beaucoup plus rapide.
Les auteurs de \cite{Bre2016ResidentialBD} ont étudié l'optimisation d'une maison familiale en combinant EnergyPlus et l'algorithme d'optimisation NSGA-II, et proposent une analyse de la sensibilité en utilisant la méthode Morris screening.
Une analyse similaire est présentée dans \cite{Recht2014AnalyseDL}, accompagnée d'une analyse d'incertitude sur le simulateur COMFIE.
Ses performances sont évaluées sur un bâtiment passif.

Parmi ces outils disponibles, Oze-Energies a choisi TRNSYS pour l'étendue de ses fonctionnalités, sa flexibilité et sa popularité.
Cependant, TRNSYS ne peut être exécuté que sur le système d'exploitation Windows, et nécessite le lancement d'une fenêtre pour chaque simulation, ce qui induit un temps de calcul d'au moins quelques secondes.

\paragraph{Paramètres de simulation. }
Dans ce paragraphe, nous détaillons les paramètres de simulation nécessaires à l'exécution de TRNSYS.
Bien que la majorité de ces paramètres soient communs à tous les bâtiments, les variables présentées ci dessous correspondent à deux bâtiments spécifiques gérés par Oze-Energies, que nous introduisons plus tard dans la Section~\ref{sec:calib:optim}.
Nous traçons un échantillon des données disponibles dans la Figure~\ref{fig:chap1fr-input_variables}, à titre de comparaison un échantillon d'une simulation réalisée par TRNSYS est affiché dans la Figure~\ref{fig:chap1fr-trnsys_simulation}.
Merci de se référencer à l'Appendice~\ref{sec:metamodel_ranges} pour une liste et une description exhaustive des variables utilisées.
Pour modéliser de futurs bâtiments, de nouvelles variables pourraient être nécessaires.
\begin{itemize}
	\item Les paramètres physiques du bâtiment, tels que la capacité thermique ou la taille des isolants, sont stockés dans un vecteur $\lambda \in \mathbb{R}^{d_\lambda}$, où $d_\lambda$ est le nombre de paramètres disponibles.
	      $\Lambda$ est l'ensemble des jeux de paramètres physiques possibles.
	\item Les usages sont décrits à travers une série temporelle, qui contient les paramètres de HVAC à chaque heure.
	      Soit $T$ la durée d'un échantillon en heures, nous définissons les usages comme $(\psi_k)_{1 \leq k \leq T} \in \mathbb{R}^{d_\psi}$. Ici, $d_\psi$ est le nombre de variables individuelles, telle que les horaires d'activation de la ventilation, les températures de chauffage.
	      $\Psi$ est l'ensemble des paramètres de HVAC possibles.
	\item De la même manière, l'occupation du bâtiment est encodée dans une série temporelle $(\delta_k)_{1 \leq k \leq T} \in \mathbb{R}$ qui dénote la fraction des occupants présents dans le bâtiment à chaque heure.
	      $\Delta$ est l'ensemble des horaires d'occupation possibles.
	\item Les conditions météorologiques sont agrégées dans la série temporelle $(\varphi_k)_{1 \leq k \leq T} \in \mathbb{R}^{d_\varphi}$.
	\item Enfin, les consommations sont notées $(\zeta_k)_{1 \leq k \leq T} \in \mathbb{R}^{d_\zeta}$, et la température intérieure $(\tau_k)_{1 \leq k \leq T} \in \mathbb{R}$.
	      $\mathcal{Z}$ (resp. $\mathcal{T}$) est l'ensemble des consommations possibles (resp. températures intérieures).
\end{itemize}
Dans la suite de ce document, nous désignons par $\varphi$ (resp. $\psi, \delta, \zeta, \tau$) les séries temporelles complètes $(\varphi_k)_{k=1}^T$ (resp. $(\psi)_{k=1}^T, (\delta)_{k=1}^T, (\zeta)_{k=1}^T, (\tau)_{k=1}^T$).
Les simulateurs physiques, tels que TRNSYS, peuvent être décrits comme des fonctions prenant comme paramètres d'entrée les propriétés du bâtiment, ses usages et son occupation pour la période considérée, ainsi que les données météorologiques associées, afin de prédire (entre autres) la température intérieure horaire et les diverses consommations.
$$
	f_\texttt{building}: (\lambda, \psi, \delta, \varphi) \mapsto (\hat \tau, \hat \zeta).
$$

\begin{figure}[htpb]
	\centering
	\caption{Sur cette figure, nous traçons un échantillon des données disponibles d'occupation $\delta$ et météorologiques $\varphi$.}
	\includegraphics[width=0.8\textwidth]{images/oze/input_variables.png}
	\label{fig:chap1fr-input_variables}
\end{figure}

\begin{figure}[htpb]
	\centering
	\caption{Échantillons simulés à partir de TRNSYS, nous affichons un extrait des consommations $\zeta$ et de la température intérieure $\tau$.}
	\includegraphics[width=\textwidth]{images/oze/TRNSYS_simulation.png}
	\label{fig:chap1fr-trnsys_simulation}
\end{figure}

\paragraph{Intégration dans la solution R\&D existante. }
Lors du début des travaux présentés dans ce document, TRNSYS était déjà utilisé dans la solution développée par l'équipe R\&D, et les tâches de calibration et d'optimisation, décrites plus bas, étaient déjà implémentées.
Cependant, elles entraînaient de long temps de calcul, dûs aux nombreux appels nécessaire à TRNSYS, ainsi qu'à la nécessité de paramétrer chaque bâtiment individuellement.
De plus, TRNSYS étant un logiciel expert, nous ne pouvons interpréter ou modifier sa représentation interne du bâtiment, en d'autre termes TRNSYS est, pour nous, une boîte noire.
Nous ne pouvons donc pas utiliser les données réelles, récoltées par Oze-Energies, afin d'améliorer le modèle, ou de prendre en compte les sources potentielles d'incertitude.

\subsection{Calibration et optimisation.}
Oze-Energies vise à réduire la consommation énergétique des bâtiments, tout en maintenant leur niveau de confort.
Le département de R\&D propose des solutions algorithmiques afin d'assister les experts de la thermique dans la génération de scénarios associés à un bâtiment, ainsi que dans leur comparaison.
Dans cette section, nous détaillons ce processus de calibration, puis d'optimisation.

\paragraph{Calibration. }
Afin de pouvoir générer des courbes de températures intérieures et les consommations associées du bâtiment, nous avons besoin des paramètres physiques $\lambda$, ainsi que des usages $(\psi_k)_{1 \leq k \leq T}$ décrits dans la section précédente.
Puisque ces paramètres sont en général inconnus, nous devons les estimer.

Nous avons tout d'abord étudié différentes approches pour estimer certains paramètres spécifiques.
Par exemple, la capacité thermique d'un bâtiment peut être approchée en estimant la pente de l'évolution de la température intérieure ; de manière similaire, en analysant les courbes de consommation, on peut repérer les horaires de démarrage des HVAC, et donc en déduire les usages.
Cependant, estimer chaque paramètre de cette manière est non seulement beaucoup trop coûteux, mais risque de surcroit de mener vers une modélisation biaisée du bâtiment.
On notera que les experts thermiciens de Oze-Energies pourrait analyser directement le bâtiment sur place, mais encore une fois cela engendrerait des coûts trop importants.

Nous proposons plutôt d'estimer tous les paramètres inconnus en même temps, à travers une procédure de calibration automatique, qui consiste à minimiser une fonction de coût qui associe, avec chaque jeu de paramètres, la différence entre une simulation du modèle et les données historiques du bâtiment, voir par exemple \cite{Coakley2014ARO,Corff2018OPTIMIZINGTC}:
\begin{equation}
	\mathcal{L}_{\texttt{calib}}(\lambda, \psi, \delta) = \mathcal{L}^\texttt{calib}_\texttt{temperature}(\tau, \hat \tau) + \beta \mathcal{L}^{\texttt{calib}}_{\texttt{energy}}(\zeta_k, \hat \zeta_k)
	\label{eq:calib}
\end{equation}
où $\hat \tau, \hat \zeta = f_\texttt{building}(\lambda, \psi, \delta, \varphi)$ et $\beta$ est une paramètres d'échelle.
Comme présenté dans \cite{Nagpal2019AMF}, cette méthodologie fournie des résultats précis pour de grande variété de bâtiments.

Nous déterminons un jeu de paramètres en utilisant des algorithmes génétiques, qui font partie des méthodes les plus efficaces pour la minimisation de fonctions non dérivables.
Dans \cite{Aird2016APPLICATIONOA}, les auteurs démontrent l'utilité du Non-dominated Sorting Genetic Algorithm II (NSGA-II) afin de sélectionner un jeu de paramètres estimés minimisant à la fois un coefficient de variation sur l'erreur quadratique, ainsi que son biais.
Ces critères peuvent aussi être combinés afin de se concentrer sur des méthodes d'optimisation à un seul objectif.
Dans \cite{Corff2018OPTIMIZINGTC}, l'algorithme CMA-ES, introduit par \cite{igel:hansen:roth:2007}, est utilisé pour minimiser une combinaison d'erreurs liées à la prédiction de la consommation du chauffage et de la climatisation.

\paragraph{Optimisation. }
L'optimisation consiste à déterminer un jeu de paramètres qui mènera à une baisse de la consommation, tout en maintenant le niveau de confort.
Pour cela, nous pouvons jouer sur les paramètres d'usage : par exemple, en décalant les horaires de démarrage de la climatisation et du chauffage, ou en les réduisant pendant la nuit et le week-end.
Nous considèrerons ici qu'une consommation plus faible correspond à une diminution de la consommation totale du bâtiment, sommée sur la fenêtre temporelle étudiée.
On notera donc que nous ne prenons pas en compte les pics de consommation, ou l'évolution du prix du kilowattheure pendant la journée.
Le confort est quant à lui défini comme la différence quadratique entre la température intérieure du bâtiment, et une température cible notée $T^\star$, pendant les périodes d'occupation.
Nous définissons les critères suivants :
\begin{equation}
	\mathcal{L}^\texttt{optim}_\texttt{temperature}(T^\star, \hat \tau) \quad \text{et} \quad \mathcal{L}^\texttt{optim}_\texttt{energy} (\hat \zeta)
\end{equation}
où $\hat \tau, \hat \zeta = f_\texttt{building}(\lambda, \psi, \delta, \varphi)$.

Contrairement à la tâche de calibration, nous devons ici résoudre un problème d'optimisation bi-objectif, car il n'existe pas de jeu de paramètres $\psi^\star$ permettant de minimiser les deux objectifs de consommation et de confort conjointement.
En effet, si une telle solution existait, on pourrait toujours améliorer un peu plus l'un des deux critères en dégradant l'autre, par exemple on peut toujours réduire un peu plus la consommation de chauffage, mais cela mènera à une dégradation du confort.
Nous cherchons donc un ensemble de paramétrages équivalents, permettant un compromis optimal entre les deux objectifs, que l'on formalise en front de Pareto.

Un jeu de paramètres $\psi^\star \in \Psi$ en domine (Pareto) un second $\psi \in \Psi$ s'il vérifie les deux conditions suivantes :
\begin{enumerate}
	\item $\mathcal{L}^\texttt{optim}_\texttt{\textcolor{blue}{temperature}}(\psi^\star) \leq \mathcal{L}^\texttt{optim}_\texttt{\textcolor{blue}{temperature}}(\psi)\; \texttt{et}\; \mathcal{L}^\texttt{optim}_\texttt{\textcolor{orange}{energy}}(\psi^\star) \leq \mathcal{L}^\texttt{optim}_\texttt{\textcolor{orange}{energy}}(\psi)$,
	\item $\mathcal{L}^\texttt{optim}_\texttt{\textcolor{blue}{temperature}}(\psi^\star) > \mathcal{L}^\texttt{optim}_\texttt{\textcolor{blue}{temperature}}(\psi)\; \texttt{ou}\; \mathcal{L}^\texttt{optim}_\texttt{\textcolor{orange}{energy}}(\psi^\star) > \mathcal{L}^\texttt{optim}_\texttt{\textcolor{orange}{energy}}(\psi)$.
\end{enumerate}
Une solution $\psi^\star \in \Psi$ est alors optimale s'il n'existe aucune autre solution qui la domine.
Le front de Pareto et l'ensemble des solutions optimales.

Sur l'illustration de la Figure~\ref{fig:pareto}, chaque point de l'espace représente la consommation totale et le niveau de confort correspondant à un jeu de paramètres.
Le front de Pareto, représenté par les points tracés en bleu, divise l'espace en deux parties : au dessus du front se trouvent les compromis sous optimaux entre consommation et confort ; la zone en dessous n'est pas atteignable dans le contexte de nos simulations.

\subsection{Qualité de l'air}
Nous nous intéressons maintenant à l'impact de l'air intérieur sur les conditions d'hygiène et de confort, à travers l'analyse de la qualité de l'air.
Parmi les nombreux facteurs qui y jouent un rôle, détaillés dans \cite{Zhang2021LowCM}, nous n'étudions que les grandeurs mesurées par Oze-Energies : la température intérieure, la concertation de \coo et l'humidité relative.

Il a été montré que la qualité de l'air a un impact fort sur le confort intérieur, et peut mener à de mauvaises conditions d'hygiène.
Par exemple, de fortes concentrations de \coo, dues en général à une surpopulation dans une pièce close en l'absence de ventilation, peut être la source de maladies telles que le Sick Building Syndrome détaillé dans \cite{Hou2021}.
Un air trop ou pas assez humide mène au développement de bactéries, virus et champignons, voir le graphique de Sterling en  Figure~\ref{fig:airq:sterling-chart}.

La modélisation de la qualité de l'air est un sujet central tout au long de cette thèse.
Dans le Chapitre~\ref{chap:metamodel}, nous optimisons le confort lié à la température intérieure, afin qu'elle ne dépasse pas des limites prédéfinies.
Dans les Chapitre~\ref{chap:uncertainty-modelling} et Chapitre~\ref{chap:latent-distributions}, nous modélisation l'évolution de l'humidité relative.

\section{Les motivations derrière la modélisation statistique}
\subsection{Métamodélisation de TRNSYS à travers des méthodes d'apprentissage profond}
Nous avons pour objectif de répliquer les simulations du logiciel TRNSYS à travers un modèle statistique qui serait à la fois plus rapide et plus flexible.
En effet, TRNSYS est particulièrement lent à opérer.
Si les quelques secondes nécessaires à son lancement ne posent en général pas de problème aux experts, elles représentent un temps colossal lorsque nous effectuons nos tâches d'optimisation itératives (la calibration et l'optimisation).
En revanche, les modèles statistiques modernes sont capable de profiter des dernières avancées techniques dans le domaine du hardware, en particulier les cartes graphiques (GPU pour Graphical Processing Unit) qui leur permettent de calculer des dizaines de simulations en parallèle.
De plus, alors que TRNSYS est une boîte noire d'un point de vue statistique, le métamodèle que nous proposons permet, à terme, de modéliser l'incertitude liée aux prédictions, comme détaillé dans les Chapitre~\ref{chap:uncertainty-modelling} et Chapitre~\ref{chap:latent-distributions}.

Une approche naïve pour modéliser le comportement d'un bâtiment consisterait à assembler une base à partir des données historiques disponibles, puis entraîner un modèle statistique tel qu'un réseau de neurones sur cette base.
Cependant, les données disponibles se sont révélées trop bruitées et clairsemées pour permettre un entraînement correct, vis à vis de la tâche à réaliser.
Nous proposons la méthodologie suivante :

\begin{enumerate}
	\item \textbf{Entraînement d'un métamodèle}: en générant divers scénarios de bâtiments (propriétés physiques, usages, conditions météorologiques) à partir de TRNSYS, pour construire une base de données synthétique sur laquelle nous entraînons les différents modèles présentés dans ce manuscrit.
	\item \textbf{Calibration et optimisation}: le métamodèle remplace TRNSYS lors des tâches de calibration et d'optimisation pour permettre d'en réduire largement le temps de calcul.
	\item \textbf{Amélioration du métamodèle}: nous pouvons estimer l'incertitude du modèle à partir des données historiques disponibles.
\end{enumerate}

\paragraph{Réseaux de neurones profonds. }
Les méthodes d'apprentissage profond visent à exploiter des quantités de données de plus en plus volumineuses.
Elles consistent à combiner des fonctions paramétriques non linéaires différentiables, nommées couches, pour résoudre  une tâche de classification ou de régression  à l'aide d'une fonction de coût.
Les paramètres des couches sont estimés de manière itérative par descente de gradient.

Les réseaux de neurones profonds ont remplacé la plupart des architectures traditionnelles dans beaucoup de champs d'application tels que la vision par ordinateur, le traitement naturel du langage ou encore la prévision de séries temporelles.
Parce qu'ils sont au centre d'un champ de recherche qui évolue rapidement, et pour les applications diverses qu'ils permettent, beaucoup des modèles présentés dans ce manuscrit sont basés sur des architectures de réseaux de neurones.

\subsection{Estimation de l'incertitude}
Les réseaux de neurones profonds sont souvent prisés pour leur capacité à estimer des millions de paramètres à partir de gigantesques bases de données, et ainsi de résoudre
des tâches complexes.
C'est pourquoi nous nous en sommes inspiré pour développer l'architecture du métamodèle.
En effet, le comportement de TRNSYS est très complexe, fortement non linéaire et traite des vecteurs d'entrée et de sortie de grande dimension.
Cependant, les réseaux de neurones produisent aussi des prévisions incorrectes, qu'il n'est pas aisé de discerner.
Puisque nous ne pouvons pas simplement nous reposer sur les capacités du métamodèle à être précis pour gérer correctement la consommation et le confort dans un bâtiment, notre travail nous mène à modéliser l'incertitude des modèles statistiques que nous développons.

Nous proposons de quantifier l'incertitude associée à une prédiction en modélisant la distribution des observations.
Si cela ne permettra sans doute pas d'améliorer les performances, il sera tout de même possible d'interpréter la distribution prédite afin de quantifier l'incertitude.
Par exemple, si cette distribution semble Gaussienne avec une petite variance, on pourra considérer la prédiction du modèle crédible.
Si la distribution semble bimodale, la prédiction associée ne permet probablement pas d'apprécier correctement les variables estimées.

Après avoir développé le métamodèle dans le Chapitre~\ref{chap:metamodel}, nous explorons deux méthodes de quantification de l'incertitude dans le Chapitre~\ref{chap:uncertainty-modelling}, avant de détailler et d'étendre la seconde dans le Chapitre~\ref{chap:latent-distributions}.

\subsection{Changement de régime pour les modèles à états cachés discrets}
Dans cette thèse, nous modélisons l'évolution de variables liées à un bâtiment à travers des modèles à état cachés, qui reposent sur une représentation interne des données pour modéliser les observations.
Si ces états cachés sont souvent modélisés comme des variables continues, nous présentons aussi des modèles à états cachés discrets, et cela pour deux raisons principales.
Tout d'abord ils peuvent permettre de simplifier la procédure de l'entraînement, car on peut alors s'affranchir de certaines approximations complexes nécessaires dans le cas continu.
De plus, ils conviennent particulièrement bien à la modélisation des problèmes de changement de régimes, comme nous le détaillons maintenant.

Représenter l'évolution d'un bâtiment à travers un nombre fini d'états cachés peut grandement en simplifier la modélisation, sans nécessairement sacrifier les performances.
Par exemple, on peut imaginer un régime qui résumerait le comportement du bâtiment chaque matin (la température extérieure augmente, l'humidité décroît, les occupants qui arrivent participent au réchauffement du bâtiment), puis des comportements différents pour chaque saison de l'année, ou encore simplement pour différencier la semaine du week-end.

Nous démontrons l'intérêt de ces modèles à états latents discrets en les comparant à leur contrepartie continue, sur une tâche de prévision de l'humidité relative, dans le Chapitre~\ref{chap:uncertainty-modelling}.
Dans le Chapitre~\ref{chap:latent-distributions}, nous étendons notre cadre de travail à des distributions discrètes plus complexes.

\section{Contributions}
Dans cette thèse, nous présentons les contributions suivantes.
Dans le Chapitre~\ref{chap:metamodel}, nous proposons d'entraîner un métamodèle basé sur un réseau de neurones récurrent (RNN). Nous le comparons à plusieurs approches alternatives, qui illustrent que les modèles de traitement des séquences conduisent à une amélioration significative des performances par rapport aux méthodes de l'état de l'art.
Notre métamodèle est alors calibré aux données historiques réelles de deux bâtiments, afin d'illustrer la flexibilité de notre approche.
La dernière étape de notre méthodologie de bout en bout consiste à optimiser la consommation énergétique, tout en maintenant le niveau de confort.
Cette méthodologie nous permet de réduire la consommation des deux bâtiments précédemment mentionnés de 5\% et 10\%.
Les résultats présentés sont adaptés de la contribution suivante : \textit{End-to-end deep meta modelling to calibrate and optimize energy consumption and comfort}, Cohen, M. Le Corff, S., Charbit, M., Champagne, A., Nozi\`ere, G, Preda, M., Energy and Buildings, Volume 250, November 2021.

Dans le Chapitre~\ref{chap:uncertainty-modelling}, nous présentons deux approches pour modéliser l'incertitude de modèles statistiques, tels que le métamodèle, appliqué à la prévision de l'humidité relative.
Nous proposons d'abord de découpler l'apprentissage de la représentation latente des données et celui de l'incertitude, dans une procédure d'entraînement à deux étapes.
Les paramètres inconnus sont estimés en minimisant une fonction de coût déterministe, puis la dernière couche du modèle est entraînée
 à nouveau en utilisant des méthodes de Monte Carlo Séquentielles.
Les résultats présentés sont adaptés de la contribution suivante : \textit{Last layer state space model for representation learning and uncertainty quantification}, Cohen, M., Charbit, M. and Le Corff, S., 2023 IEEE Statistical Signal Processing Workshop.
Nous présentons ensuite une seconde approche basée sur un modèle à états cachés discrets.
Nous montrons ainsi que l'utilisation de régimes discrets permet une meilleure interprétabilité du modèle, sans perdre en précision.
De plus, l'estimation des paramètres, réalisée par inférence variationnelle, ne nécessite pas d'approximation complexe.
Les résultats présentés sont adaptés de la contribution suivante : \textit{Variational Discrete Latent Representation for Time Series Modelling}, Cohen, M., Charbit, M. and Le Corff, S., 2023 IEEE Statistical Signal Processing Workshop.

Dans le Chapitre~\ref{chap:latent-distributions}, nous modélisons des distributions discrètes plus complexes de l'espace latent.
Par rapport aux modèles a priori de la littérature, dont l'architecture et la complexité entraînent l'utilisation d'approximations diverses durant l'entraînement, nous proposons un cadre de travail théorique pour définir et entraîner des modèles à états latents discrets, en utilisant des ponts de diffusion.
Nous montrons que notre architecture produit des performances similaires à l'état de l'art, sur des tâches de vision par ordinateur telles que la synthèse d'images, ou la complétion d'images, et ouvre la voix à de nouvelles perspectives.
Les résultats présentés sont adaptés de la contribution suivante : \textit{Diffusion Bridges Vector Quantized Variational Autoencoders}, Cohen, M., Quispe, Q., Le Corff, S., Ollion, C., Moulines, E., Proceedings of the 39th International Conference on Machine Learning (ICML), Volume 162.

\bibliographystyle{apalike}
\bibliography{src/intro/references}
