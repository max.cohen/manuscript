\chapter{Introduction}
\section{Oze-Energies}
Oze-Energies is a French company specialized in optimizing building's energy consumption.
Global energy demand for heating, ventilation and air-conditioning in commercial or public buildings has been increasing rapidly for the past few decades.
This rising demand is at the root of the complex problem of simultaneously maintaining a satisfactory thermal comfort in buildings while reducing the environmental impact.
Through innovative and durable methods, Oze-Energies aims at improving air quality and comfort, while simultaneously reducing energy consumption, without requiring any site work.

In 2009, the building stock accounted for over 40\% of the total French energy consumption, as well as almost a quarter of greenhouse emissions (Loi Grenelle\footnote{\href{https://www.legifrance.gouv.fr/loda/id/JORFTEXT000020949548/2020-09-21/}{Loi Grenelle I, Article 3}}, Figure~\ref{fig:oze:france_consumption}).
The most straight forward strategy for reducing building's consumption consists in improving their isolation, which usually involves costly renovation works.
Despite aiming to renovate 500,000 buildings every year before 2020\footnote{\href{https://www.ecologie.gouv.fr/strategie-nationale-bas-carbone-snbc}{Stratégie Nationale Bas-Carbone (SNBC)}}\footnote{\href{https://www.legifrance.gouv.fr/loda/id/JORFTEXT000020949548/2020-09-21/}{Loi Grenelle I, Article 5}}, the actions carried out in France to this date still fall short in terms of results, as stated by the Ademe (Agency for the environnement and energy)\footnote{\href{https://www.lefigaro.fr/conjoncture/accord-de-paris-pourquoi-les-pays-ne-sont-pas-a-la-hauteur-de-leurs-engagements-20190419}{Hervé Lefebvre, head of the Climat departiement of the Ademe, 2019}}.
According to the National Low-Carbon Strategy (SNBC), the average number of yearly renovations is expected to be around 370,000 for the period 2015-2030.
In contrast, the premise of Oze-Energies is the ability to produce tailored road maps for managing buildings, without requiring any renovation work.

Building's energy consumption can be largely reduced by tuning the HVAC (Heating Ventilation and Air Conditioning) settings.
Oze-Energies targets tertiary buildings, whose occupation behaviors are usually well understood (workday hours, no occupation during the weekend), however their behaviors still differ from one to another.
This is due to a wide variety of factors, such as isolation, occupancy, or heating and cooling providers: it is one of the major technical challenges of this approach that we will detail in the following chapters.
In order to precisely model the heat exchanges of each unique building, Oze-Energies monitors their consumption and air quality by integrating environmental sensors, connected through LoRA (a secured and public network) and reporting to a centralized dataset.
By combining this data with their precise understanding of building's behavior,  experts in energy efficiency known as Energy Managers are able to produce road maps for HVAC settings.
They aim at reaching the best compromise between indoor comfort and energy consumption, while improving air quality (\coo levels, humidity, etc.).
These road maps average in a 25\% reduction in consumption, in just a few weeks.
They are delivered for a recurring subscription, usually largely covered by the energy savings generated: this is Oze-Energies' main product, OPTIMZEN®.

Oze-Energies has been exploring methods to reduce the tedious work of Energy Managers: estimating building's physical attributes, proposing new road map scenarios, quantitatively comparing them.
In particular, a numerical simulator can be configured to estimate the best management settings for a building, by predicting and comparing the impact of various policies.
This expert software, TRNSYS, is able to simulate the complex behavior of buildings based on their schematics, as well as numerous related inputs such as building occupation, HVAC settings or ambient weather.
TRNSYS is first calibrated to match the real building by a machine learning procedure, using data collected from sensors.
As previously mentioned, due to the number of heat, cold or electricity providers, this calibration step becomes more and more complex as Oze-Energies acquires new clients.
Once calibrated, TRNSYS can optimize the HVAC settings to reduce consumption while maintaining or even improving comfort in the building.

Oze-Energies was created in 2014, covers over 5 millions $m^2$ over 500 buildings, and is now part of Accenta.

\section{Optimizing comfort and consumption}
\subsection{Sensors, counters and weather data}
\label{sub:sensors_counters}

Knowledge of a building's behavior is assembled by combining various kinds of data, usually gathered directly from the site.
Therefore, the first step in managing a new building consists in setting up counters responsible for sending data back to Oze-Energies' servers every few minutes.
They can be divided in three main categories.
\begin{itemize}
	\item \textbf{Sensors} are installed by Oze-Energies Managers in various areas of the building, and collect ambient variables, such as \coo levels, humidity, or indoor temperature.
	      They typically report data every 10 minutes.
	      Because they can greatly vary from one area to another, sensor data involves a challenging aggregation problem.
	      % TODO how do we deal with aggregation ?
	\item \textbf{Counters} report the building's consumption, such as heating, cooling, or electricity.
	      We usually have access to a single time series for each variable, so no further aggregation task is required ;
	      however, the main challenge of handling counters data resides in the variety of energy providers and building usage.
	      While some providers report consumption every hour, other only give access to a single daily aggregated value, requiring a complex reconstruction task to get an hour to hour time series, usually based on usual consumption patterns.
	      In addition, counter error handling can differ, as some providers ignore outliers, while others compensate on  subsequent values.
	      Finally, usage can differ significantly from one building to another: for instance, heating can be achieved through a local heater, consuming electricity, or by connecting the building to a heat provider.
	\item \textbf{Weather data}, such as outdoor temperature and humidity, or irradiance levels, play a significant role on buildings' behaviors.
	      Historic data are available from the past few years.
	      Weather forecast are also available, and considered to be accurate up to a week forward.
	      These sensors are not setup by Oze-Energies directly, but rather by a weather forecasts company: Meteotest\footnote{https://meteotest.ch/}.
\end{itemize}
A sample of the available historic data is presented in Figure~\ref{fig:chap1-sensors-counters-weather}.

\begin{figure}[htpb]
	\centering
	\caption{We plotted week long samples of sensors (indoor temperature), counters (consumption) and weather data (Direct Normal Irradiance).}
	\includegraphics[width=0.8\textwidth]{images/oze/sensors_counters_weather.png}
	\label{fig:chap1-sensors-counters-weather}
\end{figure}

\subsubsection{Building behavior simulation with TRNSYS}
Physical simulators based on thermal propagation equations are traditionally used to describe buildings.
These transient systems simulators can predict the energy use based on a description of the building's layout, construction materials and dimensions, usage and HVAC schedules along with weather data.
Because of the sheer complexity arising from modelling such diverse systems, only a few simulators are available today.

EnergyPlus was used for instance in \cite{shabunko2018energyplus} to build three types of typical designs and to benchmark the energy performance of 400 residential buildings.
In \cite{zhao2016occupant}, the authors proposed a predictive control framework based on Matlab and EnergyPlus in order to optimize energy consumptions while meeting the individual thermal comfort preference.
In \cite{Magnier2010MultiobjectiveOO}, the authors highlighted the performance of TRNSYS as a physical simulator, as well as its limits in terms of computational speed: the authors claimed that a full optimization process would take as much as ten years, had they not replaced TRNSYS with a faster surrogate model during optimization.
The authors of \cite{Bre2016ResidentialBD} studied the optimization of a single-family house using a combination of Energy-Plus and the NSGA-II optimization algorithm, and discussed sensitivity analysis using the Morris screening method.
Likewise, the authors of \cite{Recht2014AnalyseDL} performed sensitivity and uncertainty analysis on another building simulator known as COMFIE, and displayed its modelling performance on a passive building.

Among them, Oze-Energies chose TRNSYS for its completeness, flexibility and popularity.
However, TRNSYS only runs under the Windows operating system, and requires launching a window for every single simulation, incurring a runtime of at least a few seconds.

\paragraph{Simulation parameters. }
In this section, we describe the simulation parameters used by TRNSYS.
Although the majority of these parameters are relevant for any type of buildings, the specific variables presented bellow were designed for two buildings handled by Oze-Energies, introduced later in Section~\ref{sec:calib:optim}.
We plotted a sample of the available time series in Figure~\ref{fig:input_variables}, while an example of TRNSYS simulations can be found in Figure~\ref{fig:trnsys_simulation}.
Please check Appendix~\ref{sec:metamodel_ranges} for an exhaustive list and description of all used variables.
In order to model more complex buildings in the future, introducing new parameters may be required.
\begin{itemize}
	\item Physical properties of the building, such as heat capacity, size of the isolation, etc. are stacked together and denoted by a vector $\lambda \in \mathbb{R}^{d_\lambda}$, where $d_\lambda$ is the number of parameters. $\Lambda$ is the set of all possible set of physical properties.
	\item Usage is encoded as a time series describing the state of the HVAC at each hour. Let $T$ be the length of a sample, usage is defined as $(\psi_k)_{1 \leq k \leq T} \in \mathbb{R}^{d_\psi}$. Here, $d_\psi$ is the number of individual variable, such as ventilation schedule, heating temperature, etc. $\Psi$ is the set of all possible HVAC settings.
	\item Similarly, occupancy is encoded as a time series $(\delta_k)_{1 \leq k \leq T} \in \mathbb{R}$ denoting the fraction of occupants present in the building at each hour. $\Delta$ is the set of all possible occupancy schedules.
	\item Weather conditions are gathered as a time series $(\varphi_k)_{1 \leq k \leq T} \in \mathbb{R}^{d_\varphi}$.
	\item Finally, consumptions are denoted as $(\zeta_k)_{1 \leq k \leq T} \in \mathbb{R}^{d_\zeta}$, and indoor temperature is denoted $(\tau_k)_{1 \leq k \leq T} \in \mathbb{R}$. $\mathcal{Z}$ (resp. $\mathcal{T}$) is the set of all possible consumptions (resp. indoor temperatures).
\end{itemize}
In the rest of this document, we use the short hand notation $\varphi$ (resp. $\psi, \delta, \zeta, \tau$) to denote the entire time series $(\varphi_k)_{k=1}^T$ (resp. $(\psi)_{k=1}^T, (\delta)_{k=1}^T, (\zeta)_{k=1}^T, (\tau)_{k=1}^T$).

Building simulators, such as TRNSYS, are functions taking as input the physical properties of the building, its usage and occupancy for the given period, as well as the associated weather data, and predict (among others) the hourly indoor temperature and consumptions.
$$
	f_\texttt{building}: (\lambda, \psi, \delta, \varphi) \mapsto (\hat \tau, \hat \zeta).
$$

\begin{figure}[htpb]
	\centering
	\caption{On this figure, we plotted samples from the available occupancy $\delta$ as well as weather data $\varphi$.}
	\includegraphics[width=0.8\textwidth]{images/oze/input_variables.png}
	\label{fig:input_variables}
\end{figure}

\begin{figure}[htpb]
	\centering
	\caption{Sample simulated with TRNSYS, we plotted consumptions $\zeta$ and indoor temperature $\tau$.}
	\includegraphics[width=\textwidth]{images/oze/TRNSYS_simulation.png}
	\label{fig:trnsys_simulation}
\end{figure}

\paragraph{Integration in the R\&D pipeline. } At the beginning of this work, TRNSYS was already integrated in the R\&D pipeline in order to run automated simulations, as well as as calibration and optimization tasks that are described further below.
However, this implementation suffered long computation times, linked to the numerous calls to the TRNSYS simulation function, as well as the need to manually configure each new building.
Furthermore, because TRNSYS is an expert software, we have no way to interpret or modify its internal representation of the problem, in other words TRNSYS is a black box.
Because of this, we cannot  benefit from the real data gathered in order to learn model noise or other uncertainty sources.

\subsection{Calibration and optimization.}
The aim of Oze-Energies is to reduce energy consumption while improving, or maintaining comfort.
While Energy Managers use their own knowledge and experience to reach these objectives, the R\&D department implements algorithmic solutions to provide coherent scenarios for the building, and later assist Energy Managers in their decision.
In this section, we describe the two-step process for producing these scenarios.

\paragraph{Calibration.}
Sampling indoor temperatures and consumptions associated with a given time period requires estimates of the unknown physical parameters $\lambda$ as well as usage $(\psi_k)_{1 \leq k \leq T}$ described in the previous section.

We first consider that various methods can be applied for estimating specific parameters.
For instance, the heat capacity of the building can be approximated by analyzing the slope of the indoor temperature curve ; start and stop hours of the heaters can be extrapolated from the time of increase of the consumption every morning and evening.
However, estimating each parameter on its own is not only exceedingly time consuming, but also not guaranteed to result in an  good estimation of the building behavior and these estimations are likely to be biased. % TODO add some explanation
Additionally, building experts could analyse and estimate those parameters on site, but such costly measure campaigns would have to be reiterated for every new building.

Instead, these unknown parameters may be estimated using an automatic calibration procedure, by minimizing a cost function which associates, with each set of parameters, the discrepancy between the simulation and the true consumptions and temperatures, see \cite{Coakley2014ARO,Corff2018OPTIMIZINGTC}:
\begin{equation}
	\mathcal{L}_{\texttt{calib}}(\lambda, \psi, \delta) = \mathcal{L}^\texttt{calib}_\texttt{temperature}(\tau, \hat \tau) + \beta \mathcal{L}^{\texttt{calib}}_{\texttt{energy}}(\zeta_k, \hat \zeta_k)
	\label{eq:calib}
\end{equation}
where $\hat \tau, \hat \zeta = f_\texttt{building}(\lambda, \psi, \delta, \varphi)$ and $\beta$ is a scaling parameter.
As shown in \cite{Nagpal2019AMF}, calibration yields sufficiently accurate results for a variety of different buildings.

The calibration task revolves around a non differentiable optimization problem, which is often tackled using genetic optimization methods.
In \cite{Aird2016APPLICATIONOA}, the authors demonstrate the use of the Non-dominated Sorting Genetic Algorithm II (NSGA-II) to select a set of estimated parameters that jointly minimize the coefficient of variation of the root mean square error, and the normalized mean bias error.
All criteria can instead be combined in a single calibration error, in order to turn to single objective differentiation free algorithms that offer a single best candidate, avoiding the need for further selection processes.
In \cite{Corff2018OPTIMIZINGTC}, the CMA-ES algorithm introduced in \cite{igel:hansen:roth:2007} was used to minimize a combination of heating and cooling errors.

\paragraph{Optimization.}
The optimization task aims at reaching scenarios corresponding to lower consumption and higher comfort, by changing the usage of the building, in other words by improving the handling of the HVAC: changing the start and stop time of heaters, lowering cooling during the night, etc.
We consider that lowering consumption simply corresponds to reaching a lower total consumption over the considered time frame, although it could be argued that high peak in consumption should be further penalized.
We define a comfort metric as the squared distance between indoor temperature and a target temperate $T^\star$ during occupancy hours.
From the simulation parameters defined in the previous section, we define the two following criteria:
\begin{equation}
	\mathcal{L}^\texttt{optim}_\texttt{temperature}(T^\star, \hat \tau) \quad \text{and} \quad \mathcal{L}^\texttt{optim}_\texttt{energy} (\hat \zeta)
\end{equation}
where $\hat \tau, \hat \zeta = f_\texttt{building}(\lambda, \psi, \delta, \varphi)$.

Unlike the calibration task, we are facing a bi-objective problem, as we cannot find a set of usages $\psi^\star$ that optimizes both objectives simultaneously.
For any such solution, we can always further improve one of the objectives by degrading the other, as an example reducing the heater will save energy but lower comfort, while providing the exact targeted indoor temperature throughout the day will undoubtedly increase consumption.
Instead, we search for a collection of equivalent compromises between the two objectives, formalized as a Pareto front.

A feasible solution $\psi^\star \in \Psi$ is said to Pareto dominate another solution $\psi \in \Psi$ if both:
\begin{enumerate}
	\item $\mathcal{L}^\texttt{optim}_\texttt{\textcolor{blue}{temperature}}(\psi^\star) \leq \mathcal{L}^\texttt{optim}_\texttt{\textcolor{blue}{temperature}}(\psi)\; \texttt{and}\; \mathcal{L}^\texttt{optim}_\texttt{\textcolor{orange}{energy}}(\psi^\star) \leq \mathcal{L}^\texttt{optim}_\texttt{\textcolor{orange}{energy}}(\psi)$,
	\item $\mathcal{L}^\texttt{optim}_\texttt{\textcolor{blue}{temperature}}(\psi^\star) > \mathcal{L}^\texttt{optim}_\texttt{\textcolor{blue}{temperature}}(\psi)\; \texttt{or }\; \mathcal{L}^\texttt{optim}_\texttt{\textcolor{orange}{energy}}(\psi^\star) > \mathcal{L}^\texttt{optim}_\texttt{\textcolor{orange}{energy}}(\psi)$.
\end{enumerate}
A solution $\psi^\star \in \Psi$ is then called Pareto optimal if there does not exist another solution that dominates it.
The set of Pareto optimal outcomes, is called the Pareto front.

In the illustration shown in Figure~\ref{fig:pareto}, each point in space represents the total consumption and comfort level corresponding to a single set of usage.
The Pareto front, formed by the set of drawn points, divide the space in two parts: the upper zone corresponds to sub optimal compromises between consumption and comfort ; the lower zone is not physically attainable (in the context of our simulations).

\subsection{Air quality}
The indoor air quality measures the impact of indoor air conditions on health and well being.
A wide array of chemicals and conditions are involved in its analysis, as detailed in \cite{Zhang2021LowCM}.
In this work, we will only study the variables monitored by Oze-Energies, for which we can retrieve historic data: indoor temperature, \coo concentrations and relative humidity.

Air quality has been shown to strongly impact indoor well being ; when poorly managed, indoor air can lead to unhealthy conditions.
For instance, high \coo concentrations, usually caused by having too many people enclosed in a limited space with no ventilation, can cause a disease known as Sick Building Syndrome detailed in \cite{Hou2021}.
Low and high relative humidity lead to the development of bacteria, viruses and fungi, as summarized in the Sterling Chart in Figure~\ref{fig:airq:sterling-chart}.

Air quality modelling is at the center of our focus all along this thesis.
In Chapter~\ref{chap:metamodel}, we optimize indoor temperature to remain within predefined ranges.
In Chapter~\ref{chap:uncertainty-modelling} and Chapter~\ref{chap:latent-distributions}, we model the evolution of relative humidity.

\begin{figure}[ht]
	\centering
	\caption{Sterling Chart introduced in \cite{Sterling1985Chart}.}
	\includegraphics[width=0.6\textwidth]{images/airq/sterling-chart.jpg}
	\label{fig:airq:sterling-chart}
\end{figure}

\section{Motivation for statistical modelling}
\subsection{Metamodelling TRNSYS through deep learning methods}
We aim at developing statistical models able to provide similar simulations as TRNSYS, while being both faster and more flexible.
One of the most cumbersome limitation of TRNSYS resides in its speed, or rather lack of.
If a few seconds for an Energy Manager to generate a scenario is a reasonable delay, computation times become unmanageable for the calibration and optimization tasks presented in the previous section, as they require thousands of calls to the simulation function.
Modern statistical models are able to take advantage of the most recent hardware developments, such as the Graphical Processing Units (GPU), to compute dozens of simulations in parallel, in ever shorter times.
Additionally, whereas TRNSYS is a black box from a statistical point of view, the metamodel we aim to design would be able to model uncertainty, as we detail in this document.

A naive data driven approach for modelling building's behavior could have consisted in creating a dataset from the available historic data, then training a statistical model such as a deep neural network.
However, the data was too noisy and limited in the number of samples available for a neural network to learn such a complex task.
This is why we propose the following methodology:

\begin{enumerate}
	\item \textbf{Train a metamodel}: using TRNSYS, we sample various input scenarios (building properties, usage, weather conditions, etc.) and compute the associated simulation (indoor temperature and consumption). We train the models presented in this thesis on this synthetic dataset, in order to learn a surrogate function of the TRNSYS simulator.
	\item \textbf{Perform calibration and optimization}: we can plug this metamodel to solve optimization and calibration tasks with limited computation time.
	\item \textbf{Improve the metamodel}: finally, we can leverage and develop statistical methods for estimating uncertainty on the model, using the available historic data.
\end{enumerate}

\paragraph{Deep neural networks. }
Deep learning methods aim at leveraging ever increasing amounts of data available.
They consist in combining well known layers, differentiable nonlinear parametric functions, to approximate a given task associated with a loss function.
The parameters are then updated iteratively, by gradient descent.

Neural networks have steadily replaced most traditional architectures in fields such as Computer Vision, Natural Language Processing, or time series forecasting.
Because deep learning is a quickly evolving field of research, with diverse applications, many of the models presented in this thesis are derived from neural networks.

\paragraph{Parameters estimation through automated gradient descent. }
Neural networks have recently outperformed most other statistical models by inferring up to millions of parameters through an automated gradient descent.
As all layers composing a neural network are differentiable, it is possible to compute the gradient of the loss function with respect to each parameter, as a function of the architecture of the network.
In other words, it is possible to define a training algorithm, based on gradient descent, able to estimate the parameters of a deep learning model, regardless of the number or type of layers it is composed of.
By combining this flexibility of the training procedure, with modern computing units able to parallelize computations (GPU), neural networks have been able to take advantage of arbitrary amounts of training samples.

% \subsection{Traditional building blocks}
% Most deep learning layers belong to a set of well known
% \begin{itemize}
% 	\item Fully connected layer, for no prior knowledge
% 	\item Convolution layers for CV, later adapted to time series
% 	\item RNN for time series, later improved through LSTM and GRU, presented later in this thesis.
% 	\item Attention based model, such as the Transformer, introduced much more recently.
% 	\item Non linear functions: the required non linear part of every building block
% \end{itemize}

\subsection{Uncertainty estimation}
Deep learning models are often utilized for their ability to infer millions of parameters from huge amounts of data, leading to accurate predictions during inference.
This is why we chose such architecture for the metamodel, as the behavior of TRNSYS is complex (strongly non linear, high dimensional inputs and outputs).
Yet neural networks can provide erroneous predictions, especially when explanatory variables are unknown or unavailable, and are usually over confident in doing so.
As we cannot simply rely on a neural network being correct to provide adequate air quality, our work with Oze-Energies will lead us to investigate uncertainty modelling methods.

On way to quantify the uncertainty associated with a prediction is to model the distribution of the observations, instead of simply predicting its most likely value.
Although this could hardly improve performance, one could interpret the modelled distribution in order to quantify the uncertainty of the model.
For instance, if its behavior is similar to a Gaussian with small variance, the prediction could be considered accurate.
On the other hand, if the distribution seems bimodal or uniform, the associated prediction is likely not enough to understand the underlying state of the estimated variable.

After developing the metamodel methodology in Chapter~\ref{chap:metamodel}, we explore two uncertainty estimation methods in Chapter~\ref{chap:uncertainty-modelling} and dive into the details of the last one in Chapter~\ref{chap:latent-distributions}.

\subsection{Regime switching with discrete latent models}
In this thesis, we model the evolution of building related variables mainly through hidden latent models, which rely on an internal representation of the data to model observations.
While these latent variables are often continuous, we will be utilizing discrete latent models too, for two main reasons.
Not only can they lead to simpler training procedure, as they often require less approximations in order to estimate their parameters, but they are also particularly adequate for modelling inherently discrete behaviors, such as regime switching.

Representing the evolution of a building through a finite set of states can greatly simplify our modelling task, without necessarily loosing in performance.
For instance, we could imagine a regime summarizing building's behavior each morning (outdoor temperature rising, outdoor humidity lowering, indoor heating through occupants and electronic devices increasing), different behaviors for different seasons of the year or simply to easily differentiate week days from week ends.

We demonstrate the interest behind discrete latent models by first comparing them to their continuous counterpart on a relative humidity forecasting task in Chapter~\ref{chap:uncertainty-modelling}.
In Chapter~\ref{chap:latent-distributions}, we extend our framework to more complex discrete latent distributions.

\section{Notation}
In the following paragraphs, we detail a set of recurring symbols and acronyms.
This list is not exhaustive.
\subsection{Symbols}
\paragraph{Time series.}
\begin{itemize}
	\item $y$: observations
	\item $u$: commands
	\item $x$: latent states
	\item $a_{n:m}$: the sequence $(a_n, \cdots, a_m)$
\end{itemize}

\paragraph{Deep learning.}
\begin{itemize}
	\item $\theta$: unknown parameters
	\item $w, b$: weights and biases of the models
	\item $f, g, h$: non linear parametric functions
\end{itemize}

\paragraph{Statistical modelling.}
\begin{itemize}
	\item $\mathcal{L}$: the Evidence Lower BOund, or ELBO
	\item $\mathbb{E}_q [X]$: the expectation of the random variable $X$ with probability density $q$
	\item $\Psi_{\mu, \Sigma}$: the Gaussian probability function with mean vector $\mu$ and covariance matrix $\Sigma$
	\item $\epsilon, \eta$: centered Gaussian noise
\end{itemize}

\subsection{Acronyms}
\begin{itemize}
	\item \textbf{HMM} \quad Hidden Markov Model
	\item \textbf{FFN} \quad Feed Forward Network
	\item \textbf{RNN} \quad Recurrent Neural Network
	\item \textbf{LSTM} \quad Long Short Term Memory
	\item \textbf{GRU} \quad Gated Recurrent Unit
	\item \textbf{VI} \quad Variational Inference
	\item \textbf{ELBO} \quad Evidence Lower BOund
	\item \textbf{VAE} \quad Variational Auto Encoder
	\item \textbf{VQ-VAE} \quad Vector Quantized Variational Auto Encoder
	\item \textbf{SMC} \quad Sequential Monte Carlo
	\item \textbf{MCD} \quad Monte Carlo Dropout
	\item \textbf{MSE} \quad Mean Squared Error
	\item \textbf{RMSE} \quad Root Mean Squared Error
	\item \textbf{MAE} \quad Mean Absolute Error
\end{itemize}

\section{Contributions}
In this thesis, we present the following contributions.
In Chapter~\ref{chap:metamodel}, we propose to train a metamodel based on Recurrent Neural Networks (RNN).
We compare several approaches which illustrate that sequence to sequence models can yield a significant increase in performance with respect to the alternatives previously considered in our framework.
Our metamodel, which depends on a few physical parameters, is then calibrated using real data to provide accurate predictions for two buildings, to illustrate the flexibility of this approach.
The final step of our end-to-end methodology consists in optimizing energy consumption, while maintaining a given level of comfort.
Following this methodology, we were able to train and calibrate our metamodel and to reduce the hourly consumption of two buildings by 5\% and 10\%.
The results presented are adapted from the following contribution: \textit{End-to-end deep meta modelling to calibrate and optimize energy consumption and comfort}, Cohen, M. Le Corff, S., Charbit, M., Champagne, A., Nozi\`ere, G, Preda, M., Energy and Buildings, Volume 250, November 2021.

In Chapter~\ref{chap:uncertainty-modelling}, we present two approaches to model the uncertainty of statistical models, such as the metamodel, applied to relative humidity forecasting.
We first propose to decouple representation learning from uncertainty modelling, in a two step training procedure.
The unknown parameters are estimated by minimizing a deterministic cost function, then the last layer of the architecture is finetuned using Sequential Monte Carlo (SMC) methods.
The results presented are adapted from the following contribution: \textit{Last layer state space model for representation learning and uncertainty quantification}, Cohen, M., Charbit, M. and Le Corff, S., 2023 IEEE Statistical Signal Processing Workshop.
In a second approach, we develop a model with a discrete latent representation of the data.
We show that discrete regimes allow better interpretability of the model.
Additionally, parameter estimation does not require the complex approximations that come with continuous latent vectors, and is achieved through Variational Inference.
The results presented are adapted from the following contribution: \textit{Variational Discrete Latent Representation for Time Series Modelling}, Cohen, M., Charbit, M. and Le Corff, S., 2023 IEEE Statistical Signal Processing Workshop.

In Chapter~\ref{chap:latent-distributions}, we explore more complex modelling of discrete latent spaces.
In contrast with most prior models in the literature, whose architecture and complexity entail to various implementation tricks during the training procedure, we propose a theoretically grounded framework for discrete latent models, using diffusion bridges.
We show that our architecture is consistent with state of the art performance on computer vision tasks, such as image synthesis and inpainting, and offer new perspectives.
The results presented in this chapter are adapted from the following contribution: \textit{Diffusion Bridges Vector Quantized Variational Autoencoders}, Cohen, M., Quispe, Q., Le Corff, S., Ollion, C., Moulines, E., Proceedings of the 39th International Conference on Machine Learning (ICML), Volume 162.

\bibliographystyle{apalike}
\bibliography{src/intro/references}
